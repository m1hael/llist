Changelog
=========

Release 1.4.0
-------------

- list_sort API changed and is incompatible with the previous release
- added default compare procedure
- added unit test for sorting (using ILEUnit)
- updated examples
- added example for compare procedure