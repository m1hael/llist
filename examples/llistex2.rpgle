**FREE

///
// User defined compare procedure
//
// This example shows how to code a compare procedure which compares the list
// entries on a part of the entry.
//
// @author Mihael Schmidt
// @date 2018-06-07
///


/include 'llist/llist_h.rpgle'

dcl-pr compareSourceType int(10) extproc(*dclcase);
  value1 pointer const;
  length1 int(10) const;
  value2 pointer const;
  length2 int(10) const;
end-pr;

dcl-s sourceTypes char(10) dim(14) ctdata;

dcl-ds member_t qualified template;
  library char(10);
  file char(10);
  name char(10);
  type char(10);
end-ds;


main();
*inlr = *on;


dcl-proc main;
  dcl-s list pointer;
  dcl-ds member likeds(member_t) inz;
  dcl-ds m likeds(member_t) based(mptr);
 
  list = list_create();
 
  member.library = 'SCHMIDTM';
  member.file = 'QRPGLESRC';
  member.name = 'CMSRUNM1';
  member.type = 'SQLRPGLE';
  list_add(list : %addr(member) : %size(member));

  member.library = 'SCHMIDTM';
  member.file = 'QCMDSRC';
  member.name = 'CMSRUN';
  member.type = 'CMD';
  list_add(list : %addr(member) : %size(member));
 
  member.library = 'SCHMIDTM';
  member.file = 'QRPGLESRC';
  member.name = 'CMSRUNM2';
  member.type = 'RPGLE';
  list_add(list : %addr(member) : %size(member));

  // list_sort(list : %paddr('compareSourceType'));
  list_sort(list);
  
  mptr = list_get(list : 0);
  dsply m.name;
  mptr = list_get(list : 1);
  dsply m.name;
  mptr = list_get(list : 2);
  dsply m.name;
 
  list_dispose(list);
end-proc;


dcl-proc compareSourceType export;
  dcl-pi *n int(10);
    value1 pointer const;
    length1 int(10) const;
    value2 pointer const;
    length2 int(10) const;
  end-pi;
 
  dcl-ds source1 likeds(member_t) based(value1);
  dcl-ds source2 likeds(member_t) based(value2);
  dcl-s x1 int(10);
  dcl-s x2 int(10);
 
  if (value1 = *null and value2 = *null);
    return 0;
  elseif (value1 = *null);
    return 1;
  elseif (value2 = *null);
    return -1;
  else;
    x1 = %lookup(source1.type : sourceTypes);
    x2 = %lookup(source2.type : sourceTypes);
   
    if (x1 = 0);
      x1 = *hival;
    endif;
   
    if (x2 = 0);
      x2 = *hival;
    endif;
   
    return x1 - x2;
  endif;  

end-proc;


**CTDATA sourceTypes
DSPF
PRTF
PF
LF
SQL
SQLRPGLE
RPGLE
RPG
CL
CLP
CLLE
C
CMD
DTAARA