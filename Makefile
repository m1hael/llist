#
# Build script for Linked List
#

#-------------------------------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library for the service program.
# the rpg modules and the binder source file are also created in BIN_LIB.
# binder source file and rpg module can be remove with the clean step (make clean)
BIN_LIB=QGPL

DEF=THREAD_SAFE

TGTRLS=*CURRENT

OUTPUT=*NONE

#
# User-defined part end
#-------------------------------------------------------------------------------


# CFLAGS = RPG compile parameter
RCFLAGS=OUTPUT($(OUTPUT)) OPTION(*SRCSTMT) DBGVIEW(*LIST) DEFINE($(DEF)) OPTIMIZE(*BASIC) STGMDL(*INHERIT) TGTRLS($(TGTRLS))

# CCFLAGS = C compiler parameter
CCFLAGS=OPTIMIZE(30) DBGVIEW(*LIST)

# LFLAGS = binding parameter
LFLAGS=STGMDL(*INHERIT) TGTRLS($(TGTRLS))

OBJECTS = llist llist_sort

.SUFFIXES: .rpgle

# suffix rules
.rpgle:
	system -i "CHGATR OBJ('$<') ATR(*CCSID) VALUE(1252)"
	system "CRTRPGMOD $(BIN_LIB)/$@ SRCSTMF('$<') $(RCFLAGS)"

all: clean compile bind

llist:

llist_sort:

compile: $(OBJECTS)

bind: llist.bnd
	system "CRTSRVPGM $(BIN_LIB)/LLIST MODULE($(BIN_LIB)/LLIST $(BIN_LIB)/LLIST_SORT) $(LFLAGS) EXPORT(*SRCFILE) SRCFILE($(BIN_LIB)/LLISTSRV) TEXT('Linked List')" 

llist.bnd: .PHONY
	-system "CRTSRCPF $(BIN_LIB)/LLISTSRV RCDLEN(112)"
	-system "CPYFRMIMPF FROMSTMF('llist.bnd') TOFILE($(BIN_LIB)/LLISTSRV LLIST) RCDDLM(*ALL) STRDLM(*NONE) RPLNULLVAL(*FLDDFT)"

clean:
	-system "DLTMOD $(BIN_LIB)/LLIST"
	-system "DLTMOD $(BIN_LIB)/LLIST_SORT"
	-system "DLTF $(BIN_LIB)/LLISTSRV"

dist-clean: clean
	-system "DLTSRVPGM $(BIN_LIB)/LLIST"

.PHONY:
